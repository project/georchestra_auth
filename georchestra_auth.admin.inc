<?php

/**
 * @file
 * Provides settings pages for the geOrchestra authentication module.
 */

/**
 * Administrative settings form.
 */
function georchestra_auth_admin_settings() {

  $form['georchestra_auth_sync_every_login'] = array(
    '#type' => 'radios',
    '#title' => t('Fetch geOrchestra information'),
    '#default_value' => variable_get('georchestra_auth_sync_every_login', NULL),
    '#options' => array(
      0 => 'only when a account from geOrchestra is created (i.e., the first login of a geOrchestra user).',
      1 => 'every time a geOrchestra user logs in.'
    ),
    '#weight' => -10,
  );

  $relations = variable_get('georchestra_auth_relations', array());
  $form['georchestra_auth_relations'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('geOrchestra roles mappings'),
  );

   // Role Management
  $form['georchestra_auth_roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role Mapping'),
    '#access' => user_access('administer permissions'),
    '#description' => t('You may choose to manage assignment of one or more of the roles on this Drupal site based on the values of certain CAS attributes. ' .
      'Only the roles you select to manage will be affected.  Managed roles will be assigned or revoked whenever a user logs in thru CAS (regardless of how you set the ' .
      '"Overwrite existing values" setting above).<br>' .
      'For each managed role: if the role name is present in at least one of the attributes you specify, ' .
      'the role will be granted to the user.  If the role name is not present in any of the attributes, it will be removed from the user.<br>' .
      'Roles must be specified by name (not by numeric ID, since that is site specific).<br>'),
  );

  $roles = user_roles(TRUE);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  asort($roles);

  $form['georchestra_auth_roles']['georchestra_auth_roles_manage'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles to manage'),
    '#description' => t('Which roles should be managed by these attributes.'),
    '#default_value' => variable_get('cas_attributes_roles_manage', array()),
    '#options' => $roles,
  );

  $form['georchestra_auth_roles']['georchestra_auth_roles_mapping'] = array(
    '#type' => 'textarea',
    '#title' => t('Attributes'),
    '#description' => t('List the names of the CAS attributes which may ' .
      'contain names of the managed roles.  List one attribute per line - ' .
      'for example, <strong>department</strong> or <strong>affiliations' .
      '</strong>. Do not use token syntax, as it will not be processed. ' .
      'If the name of a managed role is present in at least one of these ' .
      'attributes, that role will be given to the user; otherwise, it will ' .
      'be taken away.'),
    '#default_value' => variable_get('cas_attributes_roles_mapping', ''),
  );

  $form['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('cas'),
    '#global_types' => FALSE,
  );

  return system_settings_form($form);
}

